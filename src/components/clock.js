import React from "react";
import { BrowserView, MobileView, TabletView } from "react-device-detect";
const ClockFunction = () => {
  /*eslint-disable-next-line*/ 
  const [time, setTime] = React.useState(Date());
  React.useEffect(() => {
    const delay = setInterval(() => {
      setTime(Date());
    }, 1000);
    return () => {
      clearInterval(delay);
    };
  }, []);

  return (
    <div>
      <BrowserView>
        <h4 className="font-weight-bold center" style={{ marginTop: 10 }}>
          {new Date().toLocaleTimeString('th-TH')}
        </h4>
      </BrowserView>
      <MobileView>
        <h4
          className="font-weight-bold center"
          id="content-mobile"
          style={{ marginTop: 10 }}
        >
          {new Date().toLocaleTimeString('th-TH')}
        </h4>
      </MobileView>
      <TabletView>
        <h4
          className="font-weight-bold center"
          id="content-tablet"
          style={{ marginTop: 10 }}
        >
          {new Date().toLocaleTimeString('th-TH')}
        </h4>
      </TabletView>
    </div>
  );
};
export default ClockFunction;
