import React from "react";
import { NavLink } from "react-router-dom";
import { Container, Row, Col, Navbar, Image, Nav } from "react-bootstrap";
import vjump from "./image/vjumplight.svg";
import vjumpdark from "./image/vjumpdark.svg";

// The function that toggles between themes
const Header = () => (
  <div className="bglight">
    <Container fluid className="navbarcolor">
      <Row>
        <Col>
          <Container fluid>
            <Row>
              <Col>
                <Navbar collapseOnSelect expand="md">
                  <Navbar.Brand href="/">
                    <picture>
                      <source
                        srcSet={vjumpdark}
                        media="(prefers-color-scheme: dark)"
                      />
                      <Image
                        src={vjump}
                        height="50"
                        alt="logo"
                        className="logoneuro"
                      ></Image>
                    </picture>
                  </Navbar.Brand>
                  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                  <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto"></Nav>
                    <Nav>
                      <NavLink
                        exact
                        to="/"
                        activeClassName="active"
                        className="nounderline"
                        style={{ textDecoration: "none" }}
                      >
                        <h4 className="navbar">&nbsp;&nbsp;Home&nbsp;&nbsp;</h4>
                      </NavLink>
                      <NavLink
                        to="/project"
                        activeClassName="active"
                        className="nounderline"
                        style={{ textDecoration: "none" }}
                      >
                        <h4 className="navbar">
                          &nbsp;&nbsp;Project&nbsp;&nbsp;
                        </h4>
                      </NavLink>
                      <NavLink
                        to="/about"
                        activeClassName="active"
                        className="nounderline"
                        style={{ textDecoration: "none" }}
                      >
                        <h4 className="navbar">
                          &nbsp;&nbsp;About&nbsp;&nbsp;
                        </h4>
                      </NavLink>
                    </Nav>
                  </Navbar.Collapse>
                </Navbar>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
    </Container>
  </div>
);

export default Header;
