import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import github from "./pages/about/github.svg";
import githublight from "./pages/about/githublight.svg";
import gitlab from "./pages/about/gitlab.svg";
import facebook from "./pages/about/facebook.svg";
import ClockFunction from "./components/clock";

const Footer = () => {
  return (
    <div
      className="align-content-center navbarcolor"
      style={{ marginTop: 100 }}
    >
      <Container fluid className="center">
        <Row>
          <Col className="center">
            <ClockFunction />
            <h5 className="center">จัดทำโดย ชาญฤทธิ์ พิศิษฐ์จริง</h5>
            <h5 className="text-black center">
              © {new Date().getFullYear()} Chanrich Pisitjing
            </h5>
            <a href="https://github.com/vjumpkung">
              <picture>
                <source
                  srcSet={githublight}
                  media="(prefers-color-scheme: dark)"
                />
                <Image
                  src={github}
                  height="32"
                  id="footericon"
                  alt="github"
                ></Image>
              </picture>
            </a>
            <a href="https://gitlab.com/vjump21848">
              <Image
                src={gitlab}
                height="32"
                id="footericon"
                alt="gitlab"
              ></Image>
            </a>
            <a href="https://www.facebook.com/chanrich.pisitjing/">
              <Image
                src={facebook}
                height="32"
                id="footericon"
                alt="facebook"
              ></Image>
            </a>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
export default Footer;
