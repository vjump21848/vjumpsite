import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import { BrowserView, MobileView, TabletView } from "react-device-detect";
import vjump from "../../image/vjump.svg";
const WelcomeTitle = "Welcome to Vjump website.";
const ThaiWelcome = "ยินดีต้อนรับเข้าสู่ เว็บไซต์ของ Vjump";
const GifStayTuned = () => (
    <div align="center">
      <BrowserView>
        <Image 
          src={vjump} 
          height="300" 
          style={{ marginTop: 80 }} 
          alt="reactundraw"
        ></Image>
      </BrowserView>
      <MobileView>
        <Image
          src={vjump} 
          height="130"
          style={{ marginTop: 50 }}
          id="content-mobile"
          alt="reactundraw"
        ></Image>
      </MobileView>
      <TabletView>
        <Image
          src={vjump} 
          height="150"
          style={{ marginTop: 60 }}
          id="content-tablet"
          alt="reactundraw"
        ></Image>
      </TabletView>
    </div>
  );
const Welcome = () => (
    <div className="bglight">
      <BrowserView>
        <Container fluid className="bglight" style={{ marginTop: 100 }}>
          <Row>
            <Col>
              <h1 className="font-weight-bold title center">{WelcomeTitle}</h1>
              <p style={{ marginTop: 80 }}></p>
              <h2 className="font-weight-bold title center">{ThaiWelcome}</h2>
              <GifStayTuned />
            </Col>
          </Row>
          <Row>
            <Col></Col>
          </Row>
        </Container>
      </BrowserView>
      <MobileView>
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-mobile"
        >
          <Row>
            <Col>
              <h1 className="font-weight-bold mobiletitle center">
                {WelcomeTitle}
              </h1>
              <p style={{ marginTop: 40 }}></p>
              <h3 className="font-weight-bold center">{ThaiWelcome}</h3>
              <GifStayTuned />
            </Col>
          </Row>
        </Container>
      </MobileView>
      <TabletView>
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-tablet"
        >
          <Row>
            <Col>
              <h1 className="font-weight-bold ipadtitle center">
                {WelcomeTitle}
              </h1>
              <p style={{ marginTop: 40 }}></p>
              <h2 className="font-weight-bold center">{ThaiWelcome}</h2>
              <GifStayTuned />
            </Col>
          </Row>
        </Container>
      </TabletView>
    </div>
  );
export default Welcome