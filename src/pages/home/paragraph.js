import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import facebookreact from "../../image/facebookreact.svg";
import bootstrap from "../../image/bootstrap.svg";
import sass from "../../image/sass.svg";
import nextjs from "../../image/nextjs.svg";
import nextjslight from "../../image/nextjslight.svg";
import html5 from "../../image/html.svg";
import css from "../../image/css.svg";
import js from "../../image/js.svg";
import vscode from "../../image/vscode.svg";

const Paragraph = () => (
  <div id="paragraph">
    <Container fluid="md">
      <Row className="justify-content-center">
        <Col lg={6}>
          <h3>What is this?</h3>
          <p>
            -
            เว็บไซต์นี้จะใช้ในการเรียนรู้เครืองมือในการพัฒนาเว็บไซต์และเป็นห้องทดลอง
            Feature ก่อน Production งานจริง
          </p>
          <p>สถานะตอนนี้ version 0.7</p>
          <br></br>
        </Col>
      </Row>
      <Row>
        <Col>
          <h3 className="center">เครื่องมือที่ใช้พัฒนาเว็บไซต์ตอนนี้</h3>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col sm={2}>
          <a
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src={facebookreact}
              fluid
              alt="react facebook logo"
              loading="lazy"
              height="160"
              className="centerimg"
            />
          </a>
          <h5 className="center">React</h5>
          <p align="center">JavaScript Library</p>
        </Col>
        <Col sm={2}>
          <a
            href="https://getbootstrap.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src={bootstrap}
              fluid
              alt="bootstrap"
              loading="lazy"
              height="160"
              className="centerimg"
            />
          </a>
          <h5 className="center">Bootstrap</h5>
          <p align="center">css Framework</p>
        </Col>
        <Col sm={2}>
          <a
            href="https://sass-lang.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src={sass}
              fluid
              alt="sass"
              loading="lazy"
              height="160"
              className="centerimg"
            />
          </a>
          <h5 className="center">Sass</h5>
          <p align="center">css Preprocessor</p>
        </Col>
        <Col sm={2}>
          <a
            href="https://nextjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <picture>
              <source media="(prefers-color-scheme: light)" srcSet={nextjs} />
              <source
                media="(prefers-color-scheme: dark)"
                srcSet={nextjslight}
              />
              <Image
                src={nextjslight}
                fluid
                alt="nextjslight"
                loading="lazy"
                height="160"
                className="centerimg"
              />
            </picture>
          </a>
          <h5 className="center">Next.JS</h5>
          <p align="center">SSR JavaScript</p>
          <br />
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col sm={2}>
          <a
            href="https://www.w3schools.com/html/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src={html5}
              fluid
              alt="HTML5"
              loading="lazy"
              height="160"
              className="centerimg"
            />
          </a>
          <h5 className="center">HTML5</h5>
          <p align="center">Basic Website Language</p>
        </Col>
        <Col sm={2}>
          <a
            href="https://www.w3schools.com/css/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src={css}
              fluid
              alt="css"
              loading="lazy"
              height="160"
              className="centerimg"
            />
          </a>
          <h5 className="center">css</h5>
          <p align="center">style of an HTML</p>
        </Col>
        <Col sm={2}>
          <a
            href="https://www.w3schools.com/js/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src={js}
              fluid
              alt="js"
              rel="noopener noreferrer"
              loading="lazy"
              height="160"
              className="centerimg"
            />
          </a>
          <h5 className="center">JavaScript</h5>
          <p align="center">controller of an HTML</p>
        </Col>
      </Row>
      <br />
      <br />
      <Row className="justify-content-center">
        <Col md={2}>
          <Image
            src={vscode}
            alt="vscode"
            loading="lazy"
            height="160"
            className="centerimg"
          ></Image>
        </Col>
        <Col md={7}>
          <br />
          <h3>IDE ที่ใช้คือ Visual Studio Code</h3>
          <p>
            เพราะว่ามันเป็นโปรแกรมแก้ไขข้อความครอบจักรยาน เอ้ย! จักรวาล
            สามารถเขียน HTML css js ฯลฯ ได้โดยและมี extension
            ที่ช่วยในการเขียนเว็บให้ง่ายขึ้น และ Debug งานในตัวโปรแกรมได้เลย
            ที่สำคัญคือ Open-Source
          </p>
          <br />
        </Col>
      </Row>
    </Container>
  </div>
);
export default Paragraph;
