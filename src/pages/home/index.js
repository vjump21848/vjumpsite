import React from "react";
import Welcome from "./welcome";
import "../../loading.css";
import Paragraph from "./paragraph";
import { Helmet } from "react-helmet";

const home = () => (
  <div>
      <Helmet>
        <title>VjumpKunG App เว็บไซต์นี้ใช้ Client-Side Rendering - Home</title>
      </Helmet>
    <Welcome />
    <Paragraph />
  </div>
);

export default home;
