import React from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Image } from "react-bootstrap";
import github from "./github.svg";
import githublight from "./githublight.svg";
import gitlab from "./gitlab.svg";
import facebook from "./facebook.svg";
import reactlogo from "../../image/vjump.svg";
import { BrowserView, MobileView, TabletView } from "react-device-detect";

const Githubdarkorlight = () => {
  return (
    <div className="bglight">
      {/*PC View*/}
      <BrowserView>
        <Container fluid className="bglight" style={{ marginTop: 50 }}>
          <Row>
            <Col className="center">
              <h1 className="font-weight-bold title center">About</h1>
              <h1 className="center">Chanrich Pisitjing</h1>
              <h2 className="center">ชาญฤทธิ์ พิศิษฐ์จริง</h2>
              <div>
                <h2 className="about">
                  <picture>
                    <source
                      srcSet={githublight}
                      media="(prefers-color-scheme: dark)"
                    />
                    <Image src={github} height="60" alt="github"></Image>
                  </picture>{" "}
                  <a
                    href="https://github.com/vjumpkung"
                    className="nounderline text-gray"
                    style={{ textDecoration: "none" }}
                  >
                    : vjump21848
                  </a>{" "}
                  &nbsp;
                  <Image src={gitlab} height="60" alt="gitlab"></Image>{" "}
                  <a
                    href="https://gitlab.com/vjump21848"
                    className="nounderline text-gray"
                    style={{ textDecoration: "none" }}
                  >
                    : vjumpkung
                  </a>{" "}
                  &nbsp;
                  <Image src={facebook} height="60" alt="facebook"></Image>{" "}
                  <a
                    href="https://www.facebook.com/chanrich.pisitjing/"
                    className="nounderline text-gray"
                    style={{ textDecoration: "none" }}
                  >
                    : ชาญฤทธิ์ พิศิษฐ์จริง
                  </a>{" "}
                  &nbsp;
                </h2>
              </div>
              <Image src={reactlogo} height="450"></Image>
            </Col>
          </Row>
        </Container>
      </BrowserView>
      {/*Mobile View*/}
      <MobileView>
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-mobile"
        >
          <Row>
            <Col className="center">
              <h1 className="font-weight-bold center ipadtitle">About</h1>
              <h1 className="center">Chanrich Pisitjing</h1>
              <h2 className="center">ชาญฤทธิ์ พิศิษฐ์จริง</h2>
              <div>
                <h4>
                  <picture>
                    <source
                      srcSet={githublight}
                      media="(prefers-color-scheme: dark)"
                    />
                    <Image src={github} height="40" alt="github"></Image>
                  </picture>{" "}
                  <a
                    href="https://github.com/vjumpkung"
                    style={{ textDecoration: "none" }}
                  >
                    : vjump21848
                  </a>
                  &nbsp;
                </h4>
                <h4>
                  <Image src={gitlab} height="40" alt="gitlab"></Image>{" "}
                  <a
                    href="https://gitlab.com/vjump21848"
                    style={{ textDecoration: "none" }}
                  >
                    : vjumpkung
                  </a>
                  &nbsp;
                </h4>
                <h4>
                  <Image src={facebook} height="40" alt="facebook"></Image>{" "}
                  <a
                    href="https://www.facebook.com/chanrich.pisitjing/"
                    style={{ textDecoration: "none" }}
                  >
                    : ชาญฤทธิ์ พิศิษฐ์จริง
                  </a>
                  &nbsp;
                </h4>
              </div>
              <Image src={reactlogo} width="300"></Image>
            </Col>
          </Row>
        </Container>
      </MobileView>
      {/*iPad View*/}
      <TabletView>
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-tablet"
        >
          <Row>
            <Col className="center">
              <h1 className="font-weight-bold center ipadtitle">About</h1>
              <h1 className="center">Chanrich Pisitjing</h1>
              <h2 className="center">ชาญฤทธิ์ พิศิษฐ์จริง</h2>
              <div>
                <h2>
                  <picture>
                    <source
                      srcSet={githublight}
                      media="(prefers-color-scheme: dark)"
                    />
                    <Image src={github} height="60" alt="github"></Image>
                  </picture>{" "}
                  <a
                    href="https://github.com/vjumpkung"
                    className="nounderline text-gray"
                  >
                    : vjump21848
                  </a>{" "}
                  &nbsp;
                </h2>
                <h2>
                  <Image src={gitlab} height="60" alt="gitlab"></Image>{" "}
                  <a
                    href="https://gitlab.com/vjump21848"
                    className="nounderline text-gray"
                  >
                    : vjumpkung
                  </a>{" "}
                  &nbsp;
                </h2>
                <h2>
                  <Image src={facebook} height="60" alt="facebook"></Image>{" "}
                  <a
                    href="https://www.facebook.com/chanrich.pisitjing/"
                    className="nounderline text-gray"
                  >
                    : ชาญฤทธิ์ พิศิษฐ์จริง
                  </a>{" "}
                  &nbsp;
                </h2>
              </div>
              <Image src={reactlogo} height="300"></Image>
            </Col>
          </Row>
        </Container>
      </TabletView>
    </div>
  );
};
const Aboutrender = () => (
  <div>
        <Helmet>
          <title>
            VjumpKunG App เว็บไซต์นี้ใช้ Client-Side Rendering - About
          </title>
        </Helmet>
    <Githubdarkorlight />
  </div>
);
export default Aboutrender;
