import React from "react";
import {Helmet} from "react-helmet"
import { Container, Row, Col, Image, Button } from "react-bootstrap";
import { BrowserView, MobileView, TabletView } from "react-device-detect";
import rabbit from "../../image/rabbitsstore.png";
import moandv from "../../image/moandv.png";
import vjump from "../../image/vjumpsite.png"
import "../../bootstrap.scss"

const Mywebsite = () => (
  <div>
    <Container>
      <Row className="justify-content-center">
        <Col md={6}>
          <Image
            src={rabbit}
            fluid
            alt="rabbit"
            style={{ marginTop: 25 }}
          ></Image>
          <div className="center">
            <Button
              variant="secondary"
              size="lg"
              style={{ marginTop: 25 }}
              href="https://rabbitsstore.com"
            >
              rabbitsstore.com
            </Button>
          </div>
        </Col>
        <Col md={6}>
          <Image
            src={moandv}
            fluid
            alt="moandv"
            style={{ marginTop: 25 }}
          ></Image>
          <div className="center">
            <Button
              variant="secondary"
              size="lg"
              style={{ marginTop: 25 }}
              href="https://moandv.com"
            >
              moandv.com
            </Button>
          </div>
        </Col>
        <Row className="justify-content-center">
        <Col md={6}>
          <br/>
          <Image
            src={vjump}
            fluid
            alt="vjump"
            style={{ marginTop: 25 }}
          ></Image>
          <div className="center">
            <Button
              variant="secondary"
              size="lg"
              style={{ marginTop: 25 }}
              href="https://vjumpkung.vercel.app"
            >
              vjumpkung.vercel.app
            </Button>
          </div>
        </Col></Row>
      </Row>
    </Container>
  </div>
);

const project = () => (
  <div className="bglight">
      <Helmet>
        <title>VjumpKunG App เว็บไซต์นี้ใช้ Client-Side Rendering - Project</title>
      </Helmet>
    <BrowserView>
      <Container fluid className="bglight" style={{ marginTop: 50 }}>
        <Row>
          <Col>
            <h1 className="font-weight-bold title center">My Website</h1>
          </Col>
        </Row>
        <Row>
          <Col style={{ marginTop: 50 }}>
            <Mywebsite />
          </Col>
        </Row>
      </Container>
    </BrowserView>
    <MobileView>
      <Container
        fluid
        className="bglight"
        style={{ marginTop: 50 }}
        id="content-mobile"
      >
        <Row>
          <Col>
            <h3 className="font-weight-bold center ipadtitle">My Website</h3>
          </Col>
        </Row>
        <Row>
          <Col style={{ marginTop: 50 }}>
            <Mywebsite />
          </Col>
        </Row>
      </Container>
    </MobileView>
    <TabletView>
      <Container
        fluid
        className="bglight"
        style={{ marginTop: 50 }}
        id="content-tablet"
      >
        <Row>
          <Col>
            <h1 className="font-weight-bold center ipadtitle">My Website</h1>
          </Col>
        </Row>
        <Row>
          <Col style={{ marginTop: 50 }}>
            <Mywebsite />
          </Col>
        </Row>
      </Container>
    </TabletView>
  </div>
);
export default project;
