import React from "react";
import Header from "./header";
import Footer from "./footer";
import Routing from "./routes/index"
import "./css/light.scss"
import "./bootstrap.scss"
import "./loading.css"
import "./css/darktheme.scss"
import "./css/global.scss"

const App = () => {
  return (
    <>
      <Header />
      <Routing />
      <Footer />
    </>
  );
};
// App.js
export default App;
