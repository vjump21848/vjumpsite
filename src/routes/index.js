import React from 'react'
import { Switch, Route } from 'react-router-dom'
import home from '../pages/home/'
import project from '../pages/project/'
import about from '../pages/about/'

const Routing = () => (
  <Switch>
    <Route exact path="/" component={home} />
    <Route exact path="/about" component={about} />
    <Route exact path="/project" component={project} />
  </Switch>
)
export default Routing